"""
..
  Copyright 2020 The University of Sydney

  -outfmt "6 delim=@ qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp qcovs  ssciname staxid stitle"

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import bioturbine.parser.outfmtcols

class OutfmtParser:

  class Outfmt:

    def __init__(self):
      self.num = None
      self.delimiter = str('\t')
      self.cols = []
      self.format = None

  def __init__(self):
    pass

  def parse(self, outfmt):
    ofm = OutfmtParser.Outfmt()
    pos = 0
    for i in filter(bool, [x.strip() for x in outfmt.split(' ') if x]):
      if pos == 0:
        ofm.num = int(i)
        if ofm.num == 10:
          ofm.delimiter = ','
        ofm.format = self.format(int(i))
      elif pos == 1 and '=' in i:
        ofm.delimiter = str(i.split('=')[1])
      else:
        if i in bioturbine.parser.outfmtcols.OUTFMTCOLS:
          ofm.cols.append(i)
      pos += 1
    return ofm

  def format(self, opt):
    if opt in set([6, 7, 10]):
      return 'table'
    if opt in set([12, 13, 15]):
      return 'json'
    return None
