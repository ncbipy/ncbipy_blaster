"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import sys
import json


from bioturbine.storage import dbmanager
import bioturbine.components.hspanalyzer



class JsonParser:

  def __init__(self, filters=None):
    self.queries = {}
    self.hits = {}
    self.hsps = {}
    self.program = None
    self.analyzer = bioturbine.components.hspanalyzer.HspAnalyzer()
    self.filters = filters

  def parse_singlefile(self, indata):
    result = json.loads(indata)
    for i in result['BlastOutput2']:
      self.program = i['report'].pop('program')
      self.parse_results(i['report']['results'])

  def parse(self, indata):
    result = json.loads(indata)
    self.program = result['BlastOutput2']['report'].pop('program')
    self.parse_results(result['BlastOutput2']['report']['results'])

  def parse_results(self, results):
    query = self.add_query(bioturbine.components.query.BlastQuery(results['search'].pop('query_id'),
                                                                  results['search'].pop('query_title', None),
                                                                  results['search'].pop('query_len', None)))
    self.add_hits(results['search'].pop('hits'), query)

  def add_hits(self, hits, query):
    for i in hits:
      hit = bioturbine.components.hit.BlastHit(i['description'][0].pop('id'),
                                               i['description'][0].pop('accession', None),
                                               i['description'][0].pop('title', None),
                                               i.pop('len'),
                                               i.pop('taxid', None),
                                               i.pop('taxidsciname', None))
      # Filter query here
      if self.filters and 'query' in self.filters:
        if not self.keep_component('query', query, hit):
          print("Skipping query: {}".format(query.dump()), file=sys.stderr)
          self.remove_query(query)
          break
      # Filter hit here.
      if self.filters and 'hit' in self.filters:
        if not self.keep_component('hit', query, hit):
          print("Skipping hit: {}".format(hit.dump()), file=sys.stderr)
          continue
      self.add_hit(hit)
      for j in i['hsps']:
        hsp = self.get_hsp(j, query, hit)
        if self.filters and 'hsp' in self.filters:
          if not self.keep_component('hsp', query, hit, hsp):
            print("Skipping hsp: {}".format(hsp.dump()), file=sys.stderr)
            continue
          #filter HSPs here
        self.result_formatter(query, hit, self.add_hsp(self.analyzer.analyze_hsp(hsp, query, hit)))

    if not self.hsps:
      self.queries = {}
      self.hits = {}

  def keep_component(self, component, query, hit=None, hsp=None):
    for i in self.filters[component]:
      keep = i.check(query, hit, hsp)
      if keep is None:
        print("Filter for {} failed".format(component), file=sys.stderr)
      if not keep:
        return keep
    return keep

  def get_hsp(self, hspdata, query, hit):
    if self.program == 'blastn':
      return self.analyzer.analyze_hsp(bioturbine.components.blastnhsp.BlastnHsp(hspdata, query.uid, hit.uid), query, hit)

  def add_hsp(self, hsp):
    if hsp.uid not in self.hsps:
      self.hsps[hsp.uid] = hsp
    return hsp

  def add_query(self, query):
    if query.uid not in self.queries:
      self.queries[query.uid] = query
    return self.queries[query.uid]

  def add_hit(self, hit):
    if hit.uid not in self.hits:
      self.hits[hit.uid] = hit
    return self.hits[hit.uid]

  def show_queries(self):
    for i in self.queries:
      self.queries[i].dump()

  def show_hits(self):
    for i in self.hits:
      self.hits[i].dump()

  def remove_query(self, query):
    for i in self.queries:
      print(i, query.uid)
    return self.queries.pop(query.uid)

  def result_formatter(self, query, hit, hsp):
    print(json.dumps({"query":query.get_attributes(), "hit":hit.get_attributes(),
                      "hsp":hsp.get_attributes()}))
