"""
..
  Copyright 2020 The University of Sydney

  https://www.ncbi.nlm.nih.gov/data_specs/dtd/NCBI_BlastOutput2.mod.dtd

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

from bioturbine import utils
import bioturbine.sequence.blastsequence

class BlastHit(bioturbine.sequence.blastsequence.BlastSequence):

  def __init__(self, sid, accession=None, title=None, length=0, taxid=None, sciname=None):
    super().__init__('hit', sid, accession, title, length, taxid, sciname)


  def dump(self):
    return {'type':self.type, 'uid':self.uid, 'sid':self.sid,
            'title':self.title, 'length':self.length, 'taxid':self.taxid,
            'accession':self.accession, 'sciname':self.sciname}

  def get_attribute(self, attribute):
    if attribute == 'len':
      return self.length
    if attribute == 'sid':
      return self.sid
    if attribute == 'accession':
      return self.accession
    if attribute == 'title':
      return self.title
    if attribute == 'taxid':
      return self.taxid
    if attribute == 'sciname':
      return self.sciname
    return None
