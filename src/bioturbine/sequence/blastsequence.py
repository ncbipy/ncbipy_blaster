"""
..
  Copyright 2020 The University of Sydney

  https://www.ncbi.nlm.nih.gov/data_specs/dtd/NCBI_BlastOutput2.mod.dtd

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

from bioturbine import utils

class BlastSequence:

  def __init__(self, cast, sid, accession=None, title=None, length=0, taxid=None, sciname=None):
    self.cast = cast
    self.uid = utils.calc_component_uid(sid)
    self.sid = sid
    self.accession = accession
    self.title = title
    self.length = float(length)
    self.taxid = int(taxid) if taxid is not None else None
    self.sciname = sciname

  def dump(self):
    return {'cast':self.cast, 'uid':self.uid, 'sid':self.sid,
            'title':self.title, 'length':self.length, 'taxid':self.taxid,
            'accession':self.accession, 'sciname':self.sciname}

  def get_attribute(self, attribute):
    if attribute == 'len':
      return self.length
    if attribute == 'sid':
      return self.sid
    if attribute == 'accession':
      return self.accession
    if attribute == 'title':
      return self.title
    if attribute == 'taxid':
      return self.taxid
    if attribute == 'sciname':
      return self.sciname
    return None

  def get_attributes(self):
    return = self.dump()
