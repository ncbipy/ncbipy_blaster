"""
..
  Copyright 2020 The University of Sydney

  https://www.ncbi.nlm.nih.gov/data_specs/dtd/NCBI_BlastOutput2.mod.dtd

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""


from bioturbine import utils

class BlastQuery(bioturbine.sequence.blastsequence.BlastSequence):

  def __init__(self, sid, title=None, length=0):
    super().__init__('qry', sid, title, length)
    'qseqid': None,
    'qgi' : None,
    'qacc' : None,
    'qaccver' : None,
    'qlen' : None,
'qstart' : None,
  'qend' : None,
  'qseq' : None,
'qframe' : None,


  def dump(self):
    return {'type':self.type, 'uid':self.uid, 'sid':self.sid,
            'title':self.title, 'length':self.length}

  def get_attribute(self, attribute):
    if attribute == 'len':
      return self.length
    if attribute == 'sid':
      return self.sid
    if attribute == 'title':
      return self.title
    return None
