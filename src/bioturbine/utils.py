#!/usr/bin/env python3
"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import io
import os
import sys
import hashlib


def strand_to_int(strand):
  if strand == 'Plus':
    return 0
  if strand == 'Minus':
    return 1
  return None

def calc_component_uid(uiddata:str):
  return hashlib.blake2b(uiddata.encode(), digest_size=16).hexdigest()
