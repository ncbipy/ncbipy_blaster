"""
..
  Copyright 2020 The University of Sydney

  Expected filter format:
    <a> ::= hsp
    <q> ::= query
    <h> ::= hit
    <component> ::= <a>|<q>|<h>
    <attribute> ::= length|gaps|evalue ...
    <range> ::= float,float|,float|float,
    <equal> ::= float
    <ratio> ::= <component>.<attribute>*<filtervalue>
    <filtervalue> ::= <range> | <equal>
    <filter> ::= <component>.<attribute>=<filtervalue> | <component>.<attribute>=<ratio>

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import io
import os
import sys


import bioturbine.filter.componentfilter
from bioturbine.filter import numberparameter
from bioturbine.filter import ratioparameter

class FilterParser:

  component_map = {'a':'hsp', 'h':'hit', 'q':'query'}

  def __init__(self):
    self.filters = {}

  def parse(self, filters):
    for i in filters:
      values = self.get_filter_values(i)
      params = values[1].split('*')
      if len(params) == 1:
        self.add_filter(self.config_numberparameter(values[0], params[0]))
      elif len(params) == 2:
        self.add_filter(self.config_ratioparameter(values[0], params[0], params[1]))
      else:
        sys.exit("Bad filter: {}.Abort".format(i))

  def get_filter_values(self, filt):
    if filt.find('=') == -1:
      sys.exit("Bad filter parameter: {}".format(filt))
    values = filt.strip().split('=')
    if not values[1]:
      sys.exit("Bad filter parameter: {}".format(filt))
    return values

  def config_numberparameter(self, filtattrib, params):
    component, attribute = filtattrib.split('.')
    p = numberparameter.NumberParameter(self.map_component(component), attribute)
    self.set_parameter_values(p, params.split(','), filtattrib)
    return p

  def config_ratioparameter(self, filtattrib, ratio_attrib, params):
    component, attribute = filtattrib.split('.')
    r_comp, r_attr = ratio_attrib.split('.')
    p = ratioparameter.RatioParameter(self.map_component(component), attribute,
                                      self.map_component(r_comp), r_attr)
    self.set_parameter_values(p, params.split(','), filtattrib)
    return p

  def set_parameter_values(self, param, values, filtattrib):
    if len(values) > 2:
      sys.exit("filterparameter {}: bad values: {}".format(filt, ''.join(values)))
    if len(values) == 1: #equal condition
      param.equal = float(values.pop())
    if len(values) == 2 and values[0]:# min
      param.min = float(values[0])
    if len(values) == 2 and values[1]:# max
      param.max = float(values[1])

  def map_component(self, name):
    if name not in FilterParser.component_map:
      sys.exit("Bad component name: {name}.Abort".format())
    return FilterParser.component_map[name]

  def add_filter(self, param):
    if param.component not in self.filters:
      self.filters[param.component] = []
    self.filters[param.component].append(bioturbine.filter.componentfilter.ComponentFilter(param))

  def show_filters(self):
    if self.filters:
      print("Filters:", file=sys.stderr)
      for i in self.filters:
        print("\t{}:".format(i), file=sys.stderr)
        for j in self.filters[i]:
          print("\t{}".format(j.dump()), file=sys.stderr)
