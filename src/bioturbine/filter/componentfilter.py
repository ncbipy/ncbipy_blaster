"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import io
import os
import sys


class ComponentFilter:

  def __init__(self, filterparams):
    self.params = filterparams

  def check(self, query=None, hit=None, hsp=None):
    if query is None and hit is None and hsp is None:
      sys.exit("No components for filter check given. Abort")
    #print("Checking {} {} {}".format(self.params.test_type().attribute,
                                      #self.params.test_type().name,
                                      #self.params.test_type().value))
    if self.params.isRatio:
      return self.check_ratios(query, hit, hsp)
    return self.check_values(query, hit, hsp)

  def check_ratios(self, query, hit, hsp):
    lhs = self.get_component_attribute(self.params.component, self.params.attribute,
                                       query, hit, hsp)
    rhs = self.get_component_attribute(self.params.r_comp, self.params.r_attr,
                                       query, hit, hsp)
    if self.params.equal:
      return self.isEqual(lhs, rhs*self.params.equal)
    if self.params.min and self.params.max:
      return self.isBetween(lhs, rhs*self.params.min, rhs*self.params.max)
    if self.params.min:
      return self.isGreaterEqual(lhs, rhs*self.params.min)
    if self.params.max:
      return self.isLessEqual(lhs, rhs*self.params.max)
    return None

  def check_values(self, query, hit, hsp):
    lhs = self.get_component_attribute(self.params.component, self.params.attribute,
                                       query, hit, hsp)
    if self.params.equal:
      return self.isEqual(lhs, self.params.equal)
    if self.params.min and self.params.max:
      return self.isBetween(lhs, self.params.min, self.params.max)
    if self.params.min:
      return self.isGreaterEqual(lhs, self.params.min)
    if self.params.max:
      return self.isLessEqual(lhs, self.params.max)
    return None

  def get_component_attribute(self, component, attribname, query, hit, hsp):
    attribute = None
    if component == 'query' and query:
      attribute = query.get_attribute(attribname)
    if component == 'hit' and hit:
      attribute = hit.get_attribute(attribname)
    if component == 'hsp' and hsp:
      attribute = hsp.get_attribute(attribname)
    if not attribute:
      sys.exit("Attribute {} not in {}.Abort".format(attribname, component))
    return attribute

  def isEqual(self, lhs, rhs):
    #print("lhs:{}  rhs{}".format(lhs, rhs))
    return bool(lhs == rhs)

  def isBetween(self, lhs, rhs_min, rhs_max):
    #print("lhs: {} rhs: {}-{}".format(lhs, rhs_min, rhs_max))
    return bool( (lhs >= rhs_min) and (lhs <= rhs_max))

  def isGreaterEqual(self, lhs, rhs):
    #print("lhs:{} rhs:{}".format(lhs, rhs))
    return bool(lhs >= rhs)

  def isLessEqual(self, lhs, rhs):
    #print("lhs: {} rhs: {}".format(lhs, rhs))
    return bool(lhs <= rhs)

  def dump(self):
    return self.params.dump()
