"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

class FilterParameter:

  class Description:

    def __init__(self, name, value, parameter):
      self.name = name
      self.value = value
      self.attribute = "{}.{}".format(parameter.component, parameter.attribute)

  def __init__(self, component, attribute, isRatio=False):
    self.component = component
    self.attribute = attribute
    self.isRatio = isRatio
    self.min =  None
    self.max = None
    self.equal = None

  def dump_basic_attributes(self):
    return {'component':self.component, 'min':self.min, 'max':self.max,
            'attribute':self.attribute, 'test':self.test_type().name,
            'equal':self.equal}

  def test_type(self):
    if self.equal:
      return FilterParameter.Description("equality", str(self.equal), self)
    elif self.min and self.max:
      return FilterParameter.Description("range", "{}-{}".format(self.min, self.max), self)
    elif self.min and not self.max:
      return FilterParameter.Description("minimum", str(self.min), self)
    elif self.max and not self.min:
      return FilterParameter.Description("maximum", str(self.max), self)
    else:
      return FilterParameter.Description("unknown", "?", self)
