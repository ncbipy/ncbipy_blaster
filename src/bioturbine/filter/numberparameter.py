"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import bioturbine.filter.parameter

class NumberParameter(bioturbine.filter.parameter.FilterParameter):

  def __init__(self, component, attribute):
    super().__init__(component, attribute)

  def dump(self):
    return self.dump_basic_attributes()
