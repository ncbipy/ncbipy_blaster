"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import bioturbine.filter.parameter

class RatioParameter(bioturbine.filter.parameter.FilterParameter):

  def __init__(self, component, attribute, r_comp, r_attr):
    super().__init__(component, attribute, isRatio=True)
    self.r_comp = r_comp
    self.r_attr = r_attr

  def dump(self):
    data = self.dump_basic_attributes()
    data.update({'r_component':self.r_comp, 'r_attribute':self.r_attr})
    return data
