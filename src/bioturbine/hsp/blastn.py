"""
..
  Copyright 2020 The University of Sydney

  https://www.ncbi.nlm.nih.gov/data_specs/dtd/NCBI_BlastOutput2.mod.dtd

  Implementation of a BLAST HSP (High socirng pair) linking a query to a hit.
  Aligned regions should be stored as interval trees, at some point, analogous
  to NCBI's cpp toolkit [0]

  [0]: https://www.ncbi.nlm.nih.gov/IEB/ToolBox/CPP_DOC/doxyhtml/itree_8hpp_source.html#l00288
.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""


from bioturbine import utils
import bioturbine.hsp.basehsp

class BlastnHsp(bioturbine.hsp.basehsp.BaseHsp):

  def __init__(self, blast_hsp, query_uid, hit_uid):
    self.type = 'hsp'
    self.number = int(blast_hsp.pop('num'))
    self.bitscore =  float(blast_hsp.pop('bit_score'))
    self.score = float(blast_hsp.pop('score'))
    self.evalue = float(blast_hsp.pop('evalue'))
    self.identity = float(blast_hsp.pop('identity', 0))
    self.length = float(blast_hsp.pop('align_len', 0))
    self.gaps = float(blast_hsp.pop('gaps', 0))
    self.qbeg = float(blast_hsp.pop('query_from'))
    self.qend = float(blast_hsp.pop('query_to'))
    self.qstrand = utils.strand_to_int(blast_hsp.pop('query_strand', None))
    self.qseq = blast_hsp.pop('qseq', None)
    self.hbeg = float(blast_hsp.pop('hit_from'))
    self.hend = float(blast_hsp.pop('hit_to'))
    self.hstrand = utils.strand_to_int(blast_hsp.pop('hit_strand', None))
    self.hseq = blast_hsp.pop('hseq', None)
    self.qid = query_uid
    self.hid = hit_uid
    self.uid = utils.calc_component_uid(str(query_uid+hit_uid+str(self.number)))
    self.snps = 0.0
    self.mnps = 0.0
    self.singlegaps = self.gaps
    self.indels = 0.0

  def get_attribute(self, attribute):
    if attribute == 'num':
      return self.number
    if attribute == 'bit_score':
      return self.bitscore
    if attribute == 'score':
      return self.score
    if attribute == 'evalue':
      return self.evalue
    if attribute == 'identity':
      return self.identity
    if attribute == 'len':
      return self.length
    if attribute == 'gaps':
      return self.gaps
    if attribute == 'query_from':
      return self.qbeg
    if attribute == 'query_to':
      return self.qend
    if attribute == 'query_strand':
      return self.qstrand
    if attribute == 'hit_from':
      return self.hbeg
    if attribute == 'hit_to':
      return self.hend
    if attribute == 'hit_strand':
      return self.hstrand
    if attribute == 'snps':
      return self.snps
    if attribute == 'snps':
      return self.snps
    if attribute == 'mnps':
      return self.mnps
    if attribute == 'singlegaps':
      return self.singlegaps
    if attribute == 'indels':
      return self.indels
    return None

  def dump(self):
    return {'type':self.type, 'num':self.number, 'bit_score':self.bitscore,
            'score':self.score, 'evalue':self.evalue, 'identity':self.identity,
            'snps':self.snps, 'gaps':self.gaps, 'query_from':self.qbeg,
            'query_to': self.qend, 'query_strand': self.qstrand,
            'hit_from':self.hbeg, 'hit_to': self.hend, 'hit_strand':self.hstrand,
            'snps': self.snps, 'length': self.length, 'mnps': self.mnps,
            'singlegaps':self.singlegaps, 'indels':self.indels}

  def get_attributes(self):
    return self.dump()

