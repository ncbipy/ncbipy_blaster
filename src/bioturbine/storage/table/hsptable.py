"""
..
  Copyright 2019, 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""


import sqlite3
from typing import Iterable, Tuple, Type


from bioturbine.storage.table import blastresult

class HspTable(blastresult.BlastResultTable):

  def __init__(self, database):
    super().__init__('hsps', database=database)

  def create(self, connection:Type[sqlite3.Connection]) -> __qualname__:
    stmt = """CREATE TABLE IF NOT EXISTS {}
              (rid        INTEGER PRIMARY KEY,
               uid        TEXT NOT NULL,
               bitscore   FLOAT NOT NULL,
               evalue     FLOAT NOT NULL,
               identity   INT NOT NULL,
               qbeg       INT NOT NULL,
               qend       INT NOT NULL,
               qstrand    INT NOT NULL,
               hbeg       INT NOT NULL,
               hend       INT NOT NULL,
               hstrand    INT NOT NULL,
               length     INT NOT NULL,
               gaps       INT NOT NULL,
               hid        TEXT NOT NULL,
               qid        TEXT NOT NULL,
               singlegaps INT NOT NULL,
               snps       INT NOT NULL,
               mnps       INT NOT NULL,
               indels     INT NOT NULL,
               FOREIGN KEY (hid) REFERENCES hits(uid),
               FOREIGN KEY (qid) REFERENCES queries(uid),
               UNIQUE(uid,hid,qid))""".format(self.name)
    connection.cursor().execute(stmt)
    self.create_index(connection)
    return self

  def create_index(self, connection)->None:
    stmt = """CREATE UNIQUE INDEX IF NOT EXISTS {0} ON {1} (uid)""".format(self.idx, self.name)
    connection.cursor().execute(stmt)

  def get_rows(self, connection) -> Type[sqlite3.Cursor]:
    return connection.cursor().execute("SELECT * FROM {0}".format(self.name))

  def insert(self, connection, values)->None:
    stmt = """INSERT OR IGNORE INTO {0} (uid, bitscore, evalue, identity,
              qbeg, qend, qstrand, hbeg, hend, hstrand, length, gaps, hid,
              qid, snps, mnps, singlegaps, indels)
              VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)""".format(self.name)
    connection.cursor().executemany(stmt, values)
    connection.commit()
