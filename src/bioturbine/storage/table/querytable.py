"""
..
  Copyright 2019, 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""


import sqlite3
from typing import Iterable, Tuple, Type


from bioturbine.storage.table import blastresult

class QueryTable(blastresult.BlastResultTable):

  def __init__(self, database):
    super().__init__('queries', database=database)

  def create(self, connection:Type[sqlite3.Connection]) -> __qualname__:
    stmt = """CREATE TABLE IF NOT EXISTS {}
              (rid      INTEGER PRIMARY KEY,
               uid      TEXT NOT NULL,
               id       TEXT NOT NULL,
               title    TEXT,
               length   INT,
               UNIQUE(id))""".format(self.name)
    connection.cursor().execute(stmt)
    self.create_index(connection)
    return self

  def create_index(self, connection) -> None:
    stmt = """CREATE UNIQUE INDEX IF NOT EXISTS {0} ON {1} (uid)""".format(self.idx, self.name)
    connection.cursor().execute(stmt)

  def get_rows(self, connection) -> Type[sqlite3.Cursor]:
    return connection.cursor().execute("SELECT * FROM {0}".format(self.name))

  def insert(self, connection, values:Iterable[Tuple[str,int]]) -> None:
    stmt = """INSERT OR IGNORE INTO {0} (uid, id, title, length) VALUES (?,?,?,?)""".format(self.name)
    connection.cursor().executemany(stmt, values)
    connection.commit()
