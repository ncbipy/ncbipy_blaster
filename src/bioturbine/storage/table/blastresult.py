"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""


import sqlite3
from typing import Type


class BlastResultTable:

  def __init__(self, name:str, database:str):
    self.name = name
    self.database = database
    self.idx = "{}_idx".format(self.name)

  def create(self, connection:Type[sqlite3.Connection]) -> __qualname__:
    raise NotImplementedError("Implement create() method")

  def create_index(self, connection) -> None:
    raise NotImplementedError("Implement create_index() method")

  def get_rows(self, connection):
    raise NotImplementedError("Help! Implement get_rows() method")

  def insert(self, connection) -> None:
    raise NotImplementedError("Implement insert() method")

  def size(self):
    stmt = "SELECT COUNT(rid) FROM {0}".format(self.name)
    c = self.database.execute_stmt(stmt)
    return c.fetchone()[0]
