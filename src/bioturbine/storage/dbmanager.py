"""
..
  Copyright 2019, 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""


import sys
import logging
import sqlite3
from typing import Iterable, Dict, List, Tuple, Type


from bioturbine.storage.table import querytable
from bioturbine.storage.table import hittable
from bioturbine.storage.table import hsptable


class BlastResultDb:

  def __init__(self, dbpath:str):
    #self.logger = logging.getLogger(utils.resolve_log_nspace(TaxonomyDb))
    self.path = dbpath
    #self.logger.debug("{}: Create instance".format(self.path))
    self.connection = self.init_connection()
    self.querytbl = querytable.QueryTable(self.path).create(self.connection)
    self.hittbl = hittable.HitTable(self.path).create(self.connection)
    self.hsptbl = hsptable.HspTable(self.path).create(self.connection)
    #self.logger.debug("{}: Database initialized".format(self.path))

  def init_connection(self)->sqlite3.Connection:
    #self.logger.debug("{}: Connecting".format(self.path))
    connection = sqlite3.connect(self.path)
    connection.execute("PRAGMA foreign_keys=1")
    connection.execute("PRAGMA synchronous=NORMAL")
    connection.execute("PRAGMA journal_mode=WAL")
    connection.row_factory = sqlite3.Row
    #self.logger.debug("{}: Connected".format(self.path))
    return connection

  def close_connection(self)->None:
    #self.logger.debug("{}: Closing connection".format(self.path))
    self.connection.close()

  def connect(self) -> sqlite3.Connection:
    if self.connection is None:
      return self.init_connection(self.path)
    return self.connection

  def add_queries(self, queries)->None:
    self.querytbl.insert(self.connection, queries)

  def add_hits(self, hits)->None:
    self.hittbl.insert(self.connection, hits)

  def add_hsps(self, hsps)->None:
    self.hsptbl.insert(self.connection, hsps)

  def store_blast_results(self, queries, hits, hsps):
    data = []
    for i in queries:
      data.append(tuple([queries[i].uid, queries[i].qid, queries[i].title, queries[i].length]))
    self.add_queries(data)

    data = []
    for i in hits:
      data.append(tuple([hits[i].uid, hits[i].hid, hits[i].accession, hits[i].title, hits[i].length]))
    self.add_hits(data)

    data = []
    for i in hsps:
      data.append(tuple([hsps[i].uid, hsps[i].bitscore, hsps[i].evalue,
                            hsps[i].identity, hsps[i].qbeg, hsps[i].qend,
                            hsps[i].qstrand, hsps[i].hbeg, hsps[i].hend,
                            hsps[i].hstrand, hsps[i].length, hsps[i].gaps,
                            hsps[i].hid, hsps[i].qid, hsps[i].snps,
                            hsps[i].mnps, hsps[i].singlegaps, hsps[i].indels]))
    self.add_hsps(data)
