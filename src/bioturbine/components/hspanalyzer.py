"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import io
import os
import sys


class HspAnalyzer:

  def __init__(self):
    pass

  def analyze_hsp(self, hsp, query, hit):
    if hsp.identity == query.length:
      return hsp
    #print(query.qid, hit.hid, hsp.number, hsp.qseq, hsp.hseq)
    self.analyze_alignment(hsp)
    return hsp

  def analyze_alignment(self, hsp):
    i = 0
    while i < hsp.length:
      if hsp.qseq[i] == '-':
        length = self.find_indel_end(i, hsp.length, hsp.qseq.upper())
        if length > 1:
          hsp.indels += 1
          hsp.singlegaps -= length
        i += length
      elif hsp.hseq[i] == '-':
        length = self.find_indel_end(i, hsp.length, hsp.hseq.upper())
        if length > 1:
          hsp.indels += 1
          hsp.singlegaps -= length
        i += length
      elif hsp.qseq[i].upper() != hsp.hseq[i].upper():
        length = self.find_mismatch_end(i, hsp.length, hsp.qseq.upper(), hsp.hseq.upper())
        if length == 1:
          hsp.snps += 1
        if length > 1:
          hsp.mnps += 1
        if hsp.qseq[i] == '-' or hsp.hseq[i] == '-':
          length -= 1
        i += length
      else:
        i += 1

  def find_indel_end(self, pos, end, seq):
    start = pos
    while pos < end:
      if seq[pos] != '-':
        return pos-start
      pos += 1

  def find_mismatch_end(self, pos, end, seqA, seqB):
    length = 0
    while pos < end:
      if seqA[pos] == '-' or seqB[pos] == '-':
        return length
      if seqA[pos] == seqB[pos]:
        return length
      else:
        length += 1
      pos += 1
