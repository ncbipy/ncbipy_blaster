#!/usr/bin/env python3
"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import io
import os
import sys
import argparse


import bioturbine.filter.filterparser
import bioturbine.json.blastparser
import bioturbine.parser.outfmt
import bioturbine.storage.dbmanager


class Propeller:

  def __init__(self, dbpath=None):
    self.db = None
    self.outfmt = None
    if dbpath:
      self.db = bioturbine.storage.dbmanager.BlastResultDb(dbpath)

  def parse_outfmt(self, outfmt):
    p = bioturbine.parser.outfmt.OutfmtParser()
    self.outfmt = p.parse(outfmt)

  def parse(self, outmft, filters):
    p = None
    if outfmt == 'table':
      p = bioturbine.parser.table.TableParser(outfmt, filters)
    elif outfmt == 'json':
      p = bioturbine.parser.json.JsonParser(filters)

  #def harvest_json(self, jsondata, filters):
    #bp = bioturbine.json.blastparser.JsonBlastParser(filters)
    #if self.singlefile:
      #bp.parse_singlefile(jsondata)
    #else:
      #bp.parse(jsondata)
    #if self.db:
      #self.db.store_blast_results(bp.queries, bp.hits, bp.hsps)

def main():
  ap = argparse.ArgumentParser(description='Blast JSON result parser')
  ap.add_argument('-db', '--database',
                  type=str,
                  default=None,
                  metavar='<path>',
                  help="Path to local database")
  ap.add_argument('-f', '--filters',
                  type=str,
                  nargs="*",
                  default=None,
                  action="extend")
  ap.add_argument('-s', '--single',
                  action='store_true',
                  default=False,
                  help='Result is a single file json (-outfmt 15)')
  ap.add_argument('--outfmt',
                  type=str,
                  #default=argparse.SUPPRESS,
                  help='Used -outfmt parameter ')
  ap.add_argument('-o', '--output',
                  type=str,
                  default=argparse.SUPPRESS,
                  help='Path to blast output file')

  args = ap.parse_args()
  fp = bioturbine.filter.filterparser.FilterParser()
  if args.filters is not None:
    fp.parse(args.filters)
  fp.show_filters()
  p = Propeller(args.database)
  p.parse_outfmt(args.outfmt)
  print(p.outfmt.delimiter, p.outfmt.cols, p.outfmt.num, p.outfmt.format)
  #for i in sys.stdin:
    #p.harvest_json(i, fp.filters)
  return 0

if __name__ == '__main__':
  main()
