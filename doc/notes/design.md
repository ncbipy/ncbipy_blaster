# Displaying blast result design notes

Create a viewer for blast results, not a full fledged GUI.

## Step 0: Initial result overview
Blast aligns a query sequence to a subject sequence. A blast result reports such alignments using query (the query), hit(the region on the subject sequence where
the query aligned) and at least one HSP (high scoring pair). A HSP describes
the  alignment, e.g. how long, how similar, etc. A query and subject can have multiple HSP, i.e. a subject with several copies of a query sequence will have several HSPs. Therefore, a blast result can be displayed as a graph using HSPs
as edges which link the nodes hits and query.

Different criteria can be used to find interesting alignments, e.g. the
similarity between the sequences, or the length of the alignment. In addition,
taxonomic information can be added, i.e. the organism from which the query and
hit originated.

An typical blast result can describe hundreds of HSPs between different subjects and queries. To facilitate the analysis of a blast result, the alignments
can be visualized as shown in [Figure 1](../figs/alignment-result.mockup.pdf).
The similarity is indicated as edge length and the taxonomic similarity using
colors. This will give a quick overview of the result, e.g. how taxonomic
diverse is my result (numerous colors vs few colors) or where are rather good
alignments (short edges vs. long edges).

An additional feature of using different levels of detail could be to already
plot/order the alignments based on specific criteria, 

## Step 1: Detailed alignment view 
A detailed analysis, i.e. a graphical representation of the alignment can be triggered by selecting a node which looks interesting in step 0 (above). Further
considerations are to split the view into panels, e.g. four panels as outlined in [Figure 2](../figs/panel-mockup.pdf).



