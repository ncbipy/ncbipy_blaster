# README

Ongoing project to implement a simple blast result viewer. Notes about the idea can be found in `docs/notes`.

### Status
No display at all but a simple proof of concept of storing blast results as graphs.

`src/blast_parser.py < doc/examples/blast_example.json`